package com.example.covid19tracker.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.covid19tracker.databinding.CountryItemBinding
import com.example.covid19tracker.models.MyCountry
import com.squareup.picasso.Picasso

class CountriesAdapter(private val countriesList: List<MyCountry>) : RecyclerView.Adapter<CountriesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = CountryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }


    override fun getItemCount(): Int {
        return countriesList.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.d("Response", "List Count :${countriesList.size} ")
        return holder.onBind(countriesList[position])

    }
    class ViewHolder(binding: CountryItemBinding) :RecyclerView.ViewHolder(binding.root) {
        // variables bindeadas
        var ivFlag = binding.ivFlag
        var tvTitle = binding.tvTitle
        var tvCases = binding.tvCases
        var tvPopulation = binding.tvPopulation

        fun onBind(country: MyCountry) {

            val name ="Cases:${country.cases}"
            val population ="Popul.:${country.population}"
            tvTitle.text = country.country
            tvCases.text = name
            tvPopulation.text = population
            Picasso.get().load(country.countryInfo.flag).into(ivFlag)
        }

    }
}