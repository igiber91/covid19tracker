package com.example.covid19tracker.usescase.main

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.example.covid19tracker.R
import com.example.covid19tracker.adapters.CountriesAdapter
import com.example.covid19tracker.databinding.ActivityMainBinding
import com.example.covid19tracker.extras.Constants
import com.example.covid19tracker.models.MyCountry
import com.example.covid19tracker.services.CountryService
import com.example.covid19tracker.services.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        adHelp()
        loadCountries()
    }


    private fun loadCountries() {
        val country_recycler = binding.countryRecycler

        // iniciar el servicio
        val destinationService = ServiceBuilder.buildService(CountryService::class.java)
        val requestCall = destinationService.getAffectedCountryList()
        // llamada asincrona
        requestCall.enqueue(object : Callback<List<MyCountry>> {
            override fun onResponse(
                call: Call<List<MyCountry>>,
                response: Response<List<MyCountry>>
            ) {
                Log.d("Response", "onResponse: ${response.body()}")
                if (response.isSuccessful) {
                    val countryList = response.body()!!
                    Log.d("Response", "countrylist size : ${countryList.size}")
                    country_recycler.apply {
                        setHasFixedSize(true)
                        layoutManager = GridLayoutManager(this@MainActivity, 2)
                        adapter = CountriesAdapter(response.body()!!)
                    }
                } else {
                    Toast.makeText(
                        this@MainActivity,
                        "Something went wrong ${response.message()}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onFailure(call: Call<List<MyCountry>>, t: Throwable) {
                Toast.makeText(this@MainActivity, "Something went wrong $t", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }

    private fun adHelp() {
        // poner un click listener al boton de ayuda
        binding.ivHelp.setOnClickListener {
            // inicializar una nueva instancia
            val builder = AlertDialog.Builder(this@MainActivity)

            // titulo del Alert Dialog
            builder.setTitle(Constants.AD_TITLE)

            // muestra del mensaje del Alert Dialog
            builder.setMessage(Constants.AD_MSG)

            // boton del Alert Dialog
            builder.setNeutralButton(Constants.AD_OK) { _, _ ->
            }

            // crear el Alert Dialog usando un constructor
            val dialog: AlertDialog = builder.create()

            // mostrar el Alert Dialog en la interfaz
            dialog.show()
        }

    }

}


