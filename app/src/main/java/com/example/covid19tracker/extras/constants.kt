package com.example.covid19tracker.extras

object Constants {
    const val AD_TITLE = "Guide to use COVID19TRACKER"
    const val AD_MSG = "You can check the number of Covid19 cases by country and total population"
    const val AD_OK = "OK"
}